<!DOCTYPE HTML>
<html>
	<head>
		<title>Radhey Krishna Bachhar | Secure IP Network builder</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<!--[if lte IE 8]><script src="css/ie/html5shiv.js"></script><![endif]-->
		<script src="js/jquery.min.js"></script>
		<script src="js/jquery.scrollex.min.js"></script>
		<script src="js/jquery.scrolly.min.js"></script>
		<script src="js/skel.min.js"></script>
		<script src="js/init.js"></script>
		<noscript>
			<link rel="stylesheet" href="css/skel.css" />
			<link rel="stylesheet" href="css/style.css" />
			<link rel="stylesheet" href="css/style-xlarge.css" />
		</noscript>


		
	</head>
	<body>

		<!-- Header -->
			<section id="header">
				<header class="major">
					<h1>Radhey Krishna Bachhar</h1>
					<p>Secure IP Network builder </p>
				</header>
				<div class="container">
					<ul class="actions">
						<li><a href="#one" class="button special scrolly">Begin</a></li>
					</ul>
				</div>
			</section>

		<!-- One -->
			<section id="one" class="main special">
				<div class="container">
					<span class="image fit primary"><img src="images/pic01.jpg" alt="" /></span>
					<div class="content">
						<header class="major">
							<h2>Who I am</h2>
						</header>
						<p align="justify">Network engineer with extensive experience in building, managing and troubleshooting IT systems for clients of various sizes and necessities. Skills include the ability to manage all aspects of the client’s server and to work with colleagues to improve the efficiency of internal IT systems. My experience working in different capacities of network management has developed my skills and honed my attention to detail, ability to multi-task and dedication to effectiveness. I understand what a vital role the network engineer of any company plays, and I am committed to exceeding all standards in my performance. I'm currently working as IT OFFICER at Himalayan Online Service Pvt. Ltd. </p>
					</div>
					<a href="#two" class="goto-next scrolly">Next</a>
				</div>
			</section>

		<!-- Two -->
			<section id="two" class="main special">
				<div class="container">
					<span class="image fit primary"><img src="images/pic02.jpg" alt="" /></span>
					<div class="content">
						<header class="major">
							<h2>Stuff I do</h2>
						</header>
						<p>Technologies currently i'm working with</p>
						<ul class="icons-grid">
							<li>
								<span><img src="images/tech/mikrotik.png" width="50%"></span>
								<h3>Mikrotik Router Board</h3>
							</li>
							<li>
								<span><img src="images/tech/cisco.png" width="50%"></span>
								<h3>Cisco Network</h3>
							</li>
							<li>
								<span><img src="images/tech/ubuntu.png" width="50%"></span>
								<h3>Ubuntu Server</h3>
							</li>
							<li>
								<span><img src="images/tech/nagios.jpg" width="50%"></span>
								<h3>Nagios Core</h3>
							</li>
						</ul>
					</div>
					<a href="#three" class="goto-next scrolly">Next</a>
				</div>
			</section>

		<!-- Three -->
			<section id="three" class="main special">
				<div class="container">
					<span class="image fit primary"><img src="images/pic03.jpg" alt="" /></span>
					<div class="content">
						<header class="major">
							<h2>My works!!</h2>
						</header>
						<p>Projects i have worked on</p>
						<ul class="icons-grid">
							<li>
								<span><a href="nyc"><img src="images/tech/nyc.png" width="50%" alt="Nepal youth council"></a></span>
								<p>Mikrotik Wifi hotspot login page edited to user only access code to login from database. </p>
							</li>
							<li>
								<span><a href="http://cp.hons.net.np"><img src="images/tech/cphons.png" width="50%" alt="cphons"></a></span>
								<p>Invoice storage system to collect invoices with particular date and store to database.</p>
							</li>
							<li>
								<span><a href="http://jwl-nagios-01.wlink.com.np/nagios/"><img src="images/tech/nagiosjlw.png" width="80%"></a></span>
								<p>Php snmp use to get snmp-walk to collect end point log to monitor there activity.</p>
							</li>
							<li>
								<span><a href="https://classroom.hons.net.np/"><img src="images/tech/classroom.png" width="50%"></a></span>
								<p>Virtual classroom for british college to engage teachers and students virtually. </p>
							</li>
						</ul>
					</div>
					<a href="#footer" class="goto-next scrolly">Next</a>
				</div>
			</section>

			<section id="footer">
				<div class="container">
					<header class="major">
						<h2>Get in touch</h2>
					</header>
					<?php
						if(isset($_POST['name']))
						    {
						    $name = trim($_POST["name"]);
						    $email = trim($_POST["email"]);
						    $message = trim($_POST["message"]);
						    if(strlen($name)<2) {
						        print "<p>Please type your name.</p>";
						    }else if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
						        print  "<p>Please type a valid email address.</p>";
						    }else if(strlen($message)<10) {
						        print "<p>Please type your message.</p>";
						    }
						    else{ $headers =  'From: '.$email. "\r\n" .
						                  'X-Mailer: PHP/' . phpversion();
						        mail('radheykrishna1900@gmail.com',$message,$headers);
						        print "mail succesuffully sent";
						    }

						}
					?>
					<div>
						<address>
						Domainnepal Pvt Ltd<br>
						info@radheykrishna.rf.gd<br>
						9849739282<br>
						Durbarmarg, kathmandu<br>
						Nepal
						</address>

					</div>
					<form method="post" action="#">
						<div class="row uniform">
							<div class="6u 12u$(xsmall)"><input type="text" name="name" id="name" placeholder="Name" /></div>
							<div class="6u$ 12u$(xsmall)"><input type="email" name="email" id="email" placeholder="Email" /></div>
							<div class="12u$"><textarea name="message" id="message" placeholder="Message" rows="4"></textarea></div>
							<div class="12u$">
								<ul class="actions">
									<li><input type="submit" value="Send Message" class="special" /></li>
								</ul>
							</div>
						</div>
					</form>
				</div>
				<footer>
					<ul class="icons">
						<li><a href="https://www.facebook.com/mega.bachhar" class="icon alt fa-facebook"><span class="label">Facebook</span></a></li>
						<li><a href="https://www.linkedin.com/in/rbachhar/" class="icon alt fa-linkedin"><span class="label">Linkedin</span></a></li>
						<li><a href="#" class="icon alt fa-dribbble"><span class="label">Dribbble</span></a></li>
						<li><a href="#" class="icon alt fa-envelope"><span class="label">Email</span></a></li>
					</ul>
					<ul class="copyright">
					</ul>
				</footer>
			</section>

	</body>
</html>
